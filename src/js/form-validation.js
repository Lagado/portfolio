function validate() {
	var regex = /^[A-Za-z0-9_\-\.]*@.[A-Za-z0-9_\-\.]*\.[A-Za-z]{2,4}$/;
	
	var emailField = document.getElementsByClassName("email-input")[0];
	var ret = emailField.value.match(regex) != null;

	if (!ret) {
		emailField.style.backgroundColor = "#cc5500";
		emailField.value = "Please enter a valid email address";
	}

	return ret;
}