"use strict";
var tasks = [
	"sass",
	"cssmin",
	"connect",
	"watch"
];

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),

		sass: {
			dist: {
				files: {
					"css/style.css": "sass/*.scss"
				}
			}
		},
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: "src.css",
					src: ["css/style.css"],
					dest: "css/style",
					ext: ".min.css"
				}]
			}
		},
		connect: {
			options: {
				port: 6001,
				keepalive: true,
				hostname: "localhost",
				base: "."
			},
			livereload: {
				options: {
					open: true
				}
			}
		},
		watch: {
			options: {
				livereload: true
			},
			sass: {
				files: ["sass/**/*.scss"],
				tasks: ["sass", "cssmin"]
			}
		}
	});


	for (var i = 0; i < tasks.length; i++)
		grunt.loadNpmTasks("grunt-contrib-" + tasks[i]);
	

	grunt.registerTask("default", tasks);
};